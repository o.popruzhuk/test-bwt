<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel test BWT</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Nunito';
        }
    </style>
</head>
<body class="antialiased">
<div class="relative">
    <h1 class="country ml-12">{{ $country }}</h1>
    <div class="users ml-12">
        @forelse ($usersCompanyCountry as $user)
            <li>{{ $user->name }}</li>
            <table>
                <thead>
                <th>
                    Company name
                </th>
                <th>
                    Country name
                </th>
                <th>
                    Created at
                </th>
                </thead>
                <tbody>
                @foreach ($user->companies as $company)
                    <tr>
                        <td>{{ $company->name }}</td>
                        <td>{{ $company->country->name }}</td>
                        <td>{{ $company->pivot->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        @empty
            <p>No users in companies from selected country</p>
        @endforelse
    </div>
    <div class="companies ml-12">

    </div>
</div>
</body>
</html>
