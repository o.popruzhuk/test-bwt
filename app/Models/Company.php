<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model {
    use HasFactory;

    /**
     * Get Country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country() {
        return $this->belongsTo('App\Models\Country');
    }

    /**
     * Get Users.
     */
    public function users() {
        return $this->belongsToMany('App\Models\User', 'users_companies', 'company_id', 'user_id')
            ->withTimestamps();
    }
}
