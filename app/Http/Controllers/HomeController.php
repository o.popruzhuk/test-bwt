<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\User;

class HomeController extends Controller
{
    public function index()
    {
        // fetch random country name, cause we don't have exactly same country as in task description
        $country = Country::all()->random()->name;

        // fetch users with relation to companies in selected country
        $usersCompanyCountry = User::whereHas('companies.country', function ($query) use ($country) {
            $query->where('name', $country);
        })->get();

        return view('home', [
            'country' => $country,
            'usersCompanyCountry' => $usersCompanyCountry
        ]);
    }
}
