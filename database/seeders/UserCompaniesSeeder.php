<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Company;

class UserCompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // load all companies and users
        $companies = Company::all();
        $users = User::all();

        // fill dynamic relations from user to companies and reverse
        $users->each(function ($user) use ($companies) {
            $user->companies()->attach(
                $companies->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
        $companies->each(function ($company) use ($users) {
            $company->users()->attach(
                $users->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
    }
}
